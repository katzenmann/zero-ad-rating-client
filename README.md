# Adclient
This is a client for Lino's [0ad-rating](https://gitea.c3d2.de/lino/0adrating).

## Features
- Print a table of players ranked by their rating
- Predict a match outcome
- Upload game data for processing by the server

## Usage
Set the environment variable `ZERO_AD_RATING_SERVER` to the `hostname:port` of the server.
```
0 A.D. Rating Client

Usage: adclient
       adclient --predict <team1> <team2>
       adclient --upload-match-data
       adclient --help

Options:
    -p --predict <team1> <team2>        Predict match outcome between team1 and team2.
                                        Teams are a comma-separated list of players
    -u --upload-match-data [file_path]  Upload the replay of the last match to the server.
                                        If a [file_path] is set, upload that file instead.


By default adclient prints a table of players with their rating.
```
