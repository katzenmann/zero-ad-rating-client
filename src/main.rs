use data::Team;
use itertools::Itertools;
use std::{
    env::{self},
    fs::{self, File},
    path::{Path, PathBuf},
    process::exit,
};
use tabled::{settings::Style, Table};

use crate::data::{Player, Prediction};

mod data;

struct PrintPlayerTable;
struct PredictMatchOutcome(Vec<String>);
struct PostMatchData(Option<PathBuf>);

struct ExecutorTemplate {
    pub server: String,
}

impl ExecutorTemplate {
    pub fn with_command<T>(self, command: T) -> Executor<T> {
        Executor {
            server: self.server,
            command,
        }
    }
}

struct Executor<T: Sized> {
    server: String,
    command: T,
}

impl Executor<PrintPlayerTable> {
    pub fn execute(self) {
        let result = ureq::get(&format!("http://{}/player/", self.server))
            .call()
            .expect("Failed to send GET request")
            .into_string()
            .expect("Failed to decode http response text");
        let players = serde_json::from_str::<Vec<Player>>(&result)
            .expect("Failed to parse http resonse as JSON")
            .into_iter()
            .map(Player::rate)
            .sorted_by(|x, y| x.rating.total_cmp(&y.rating).reverse());

        let mut table = Table::new(players);
        table.with(Style::rounded());

        print!("{}", table);
    }
}

impl Executor<PredictMatchOutcome> {
    pub fn execute(self) {
        let PredictMatchOutcome(teams) = self.command;

        if teams.is_empty() {
            panic!("No teams provided");
        }

        let players = teams.clone().join("/");

        let result = ureq::get(&format!("http://{}/prediction/{players}", self.server))
            .call()
            .expect("Sending request failed")
            .into_string()
            .expect("Failed to parse body");

        let prediction: Prediction<Option<Team>> =
            serde_json::from_str(&result).expect("Failed to deserialize prediction from json");

        let player_teams: Vec<Vec<String>> = teams
            .into_iter()
            .map(|team| team.split(',').map(str::to_owned).collect())
            .collect();

        let prediction = prediction.validate_teams(&player_teams)
            .expect("Not all players exist on the server");

        let (winner, chance) = prediction.get_winner();
        print!("Team {} has a {chance:.1}% chance of winning!", winner.into_iter().join(","));
    }
}

fn get_last_modified_folder_in_dir(path: &Path) -> Result<PathBuf, std::io::Error> {
    let dir = fs::read_dir(path)?;

    let folder = dir
        .filter_map(|x| x.ok())
        .filter(|x| x.file_type().unwrap().is_dir())
        .sorted_by_key(|x| x.metadata().unwrap().modified().unwrap())
        .next_back()
        .ok_or(std::io::Error::from(std::io::ErrorKind::NotFound))?;

    Ok(folder.path())
}

impl Executor<PostMatchData> {
    pub fn execute(self) {
        let replay_metadata_path = if let Some(path) = self.command.0 {
            path
        } else {
            let mut data_dir: PathBuf = env::var("XDG_DATA_HOME")
                .unwrap_or_else(|_| {
                    let home = env::var("HOME").expect("Home dir not found");
                    format!("{home}/.local/share")
                })
                .into();
            data_dir.push("0ad");
            data_dir.push("replays");
            let replays_dir = get_last_modified_folder_in_dir(&data_dir)
                .expect("Failed to get replays dir");
            let mut newest_replay_dir =
                get_last_modified_folder_in_dir(&replays_dir).expect("Failed to get last replay");
            newest_replay_dir.push("metadata.json");
            newest_replay_dir
        };
        let replay_file =
            File::open(replay_metadata_path.clone()).expect("Failed to open replay metadata file");

        println!(
            "Sending replay file at {} to rating server.",
            replay_metadata_path.display()
        );

        let response = ureq::post(&format!("http://{}/game/", self.server))
            .send(replay_file)
            .expect("Failed to post game data")
            .into_string()
            .expect("Failed to decode response body");

        match response.trim() {
            "" => {
                print!("Success");
            }
            body => {
                print!("Response: {body}");
            }
        }
    }
}

fn main() {
    let mut args = env::args();
    args.next();

    let server = env::var("ZERO_AD_RATING_SERVER").unwrap_or_else(|_| {
        println!("ZERO_AD_RATING_SERVER environment variable not set. Must be hostname:port of the server");
        exit(-1);
    });

    let executor = ExecutorTemplate { server };

    match args.next().as_deref() {
        Some("-p" | "--predict") => {
            let teams = args.collect();

            executor
                .with_command(PredictMatchOutcome(teams))
                .execute();
        }
        Some("-u" | "--upload-match-data") => {
            executor
                .with_command(PostMatchData(args.next().map(PathBuf::from)))
                .execute();
        }
        Some("-h" | "--help") => print_help(),
        Some(arg) => {
            print!("Arg '{arg}' not known. Use --help for help");
        }
        None => {
            executor.with_command(PrintPlayerTable).execute();
        }
    };
}

fn print_help() {
    print!(
        "\
0 A.D. Rating Client

Usage: adclient
       adclient --predict <team1> <team2> ...
       adclient --upload-match-data
       adclient --help

Options:
    -p --predict <team1> <team2> ...    Predict match outcome between two or more teams.
                                        Teams are a comma-separated list of players
    -u --upload-match-data [file_path]  Upload the replay of the last match to the server.
                                        If a [file_path] is set, upload that file instead.


By default adclient prints a table of players with their rating.
"
    );
}
