use std::fmt::Display;

use serde::Deserialize;
use tabled::Tabled;

#[derive(Deserialize)]
pub struct PlayerRating {
    #[serde(rename = "Mu")]
    mu: f32,
    #[serde(rename = "Sigma")]
    sigma: f32,
    #[serde(rename = "Z")]
    z: f32,
}

impl PlayerRating {
    pub fn calculate(&self) -> f32 {
        self.mu - self.z * self.sigma
    }
}

#[derive(Deserialize)]
pub struct Player {
    #[serde(rename = "Name")]
    pub name: String,
    // #[serde(rename = "Rating")]
    rating: PlayerRating,
}

impl Player {
    pub fn rate(self) -> RatedPlayer {
        RatedPlayer {
            name: self.name,
            rating: self.rating.calculate(),
        }
    }
}

impl Display for Player {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.name.fmt(f)
    }
}

#[derive(Tabled)]
pub struct RatedPlayer {
    #[tabled(rename = "Name")]
    pub name: String,
    #[tabled(rename = "Rating")]
    pub rating: f32,
}

pub type Team = Vec<Player>;

#[derive(Deserialize)]
pub struct Prediction<T> {
    teams: Vec<T>,
    prediction: Vec<f32>,
}

impl Prediction<Option<Team>> {
    pub fn validate_teams(self, teams: &[Vec<String>]) -> Option<Prediction<Team>> {
        let server_teams: Vec<Team> = self.teams.into_iter().map(|team|
            team.unwrap_or_default()
        )
        .collect();

        let mut players_missing = false;
        for (server_team, local_team) in server_teams.iter().zip(teams.iter()) {
            players_missing = print_missing_players(local_team, server_team) | players_missing;
        }
        if !players_missing {
            Some(Prediction::<Team> {
                teams: server_teams,
                prediction: self.prediction,
            })
        } else {
            None
        }
    }
}

impl Prediction<Team> {
    pub fn get_winner(&self) -> (&Team, f32) {
        let (index, chance) = self
            .prediction
            .iter()
            .enumerate()
            .max_by(|x, y| x.1.partial_cmp(y.1).unwrap())
            .unwrap();
        let chance = chance * 100.;
        let winner_team = &self.teams[index];
        (winner_team, chance)
    }

}

fn print_missing_players(local_team: &[String], server_team: &[Player]) -> bool {
    let mut missing_players = local_team
        .iter()
        .filter(|x| !server_team.iter().any(|p| p.name == **x))
        .peekable();

    let players_missing = missing_players.peek().is_some();
    missing_players.for_each(|player| {
        println!("Player '{player}' does not exist on server");
    });
    players_missing
}
